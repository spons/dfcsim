import Controller from '@ember/controller';
import EmberObject from '@ember/object';
import $ from 'jquery';

export default Controller.extend({
    init() {
    this._super(...arguments);
    let store = this.get('store');
    let weaponSpecialRules = [
      {label: 'Burnthrough (3)', name: 'burnthrough-3'},
      {label: 'Burnthrough (4)', name: 'burnthrough-4'},
      {label: 'Burnthrough (6)', name: 'burnthrough-6'},
      {label: 'Burnthrough (8)', name: 'burnthrough-8'},
      {label: 'Close Action', name: 'close_action'},
    ];
    weaponSpecialRules.forEach(
      function (specialRule) {
        store.queryRecord('weapon-special-rule',
          {filter: { name: specialRule.name}}).then(
            function (foundSpecialRule) {
              if (foundSpecialRule === null) {
                store.createRecord('weapon-special-rule',
                  {name: specialRule.name, label: specialRule.label}).save();
              }
            }
        );
      }
    );
  },

  actions: {
    createWeapon(label, lock, attack, damage, simulation) {
      let weapon = this.store.createRecord('weapon', {
        label: label,
        lock: lock,
        attack: attack,
        damage: damage,
        simulation: simulation,
      });
      weapon.save();
    },

    createTarget(armor, pd, simulation) {
      let target = this.store.createRecord('target', {
        armor: armor,
        pd: pd,
        simulation: simulation,
      });
      target.save().then( () => {
        simulation.set('target', target);
      }).then( () => {
        simulation.save();
      });
      },

    createResult(damageProbability, simulation) {
      let result = this.store.createRecord('result', {
        damageProbability: damageProbability,
        simulation: simulation,
      });
      result.save().then( () => {
        simulation.set('result', result);
      }).then( () => {
        simulation.save();
      });
      },

    createSimulation() {
      let numSimulations = this.get('model.simulations.length');
      let simulationLabel = "Simulation " + (numSimulations + 1);

      let simulation = this.store.createRecord('simulation', {
        label: simulationLabel,
      });
      simulation.save().then(() => {
        this.send('createWeapon', 'Weapon 1', 4, 2, 1, simulation);
        this.send('createTarget', 4, 5, simulation);
      }).then( () => {
        simulation.save();
      });
    },

    deleteSimulation(simulation) {
      simulation.get('result').then((result) => {
        if (result != null) {
            result.destroyRecord();
          }
        });

      simulation.get('target').then((target) => {
        if (target != null) {
            target.destroyRecord();
          }
        });

      simulation.destroyRecord();
    },

    saveSimulation(simulation) {
      this.simulation = simulation;
      let self = this;
      simulation.save().then( () => {
        simulation.get('weapons').then((weapons) => {
          weapons.forEach(function (weapon) {
            weapon.save();
          });
        });
        simulation.get('target').then((target) => {
          target.save();
        });

      }).then( () => {
          let simulationWeapons = simulation.get('weapons').map(weapon =>
          EmberObject.create({
            "lock": parseInt(weapon.get('lock')),
            "attack": parseInt(weapon.get('attack')),
            "dmg": parseInt(weapon.get('damage')),
            "special": weapon.get('specialRules').map(specialRule =>
                                                      specialRule.name),
          }));

          $.ajax({
            method: 'POST',
            url: 'https://kqw5pazewc.execute-api.us-east-1.amazonaws.com/develop/dfc_roll',
            data: JSON.stringify(
              {
                "armor": parseInt(simulation.get('target.armor')),
                "pd": parseInt(simulation.get('target.pd')),
                "weapons": simulationWeapons,
              }
            ),
            contentType: 'application/json',
          }).then( function(damageProbability) {
            simulation.get('result').then((result) => {
              if (result == undefined) {
                self.send('createResult', damageProbability, simulation);
              }
              else {
                result.set('damageProbability', damageProbability);
                result.save();
              }
            });
            }
          );
      });
    },

    addWeapon(simulation) {
      let numWeapons = simulation.get('weapons.length');
      let weaponLabel = "Weapon " + (numWeapons + 1);
      this.send('createWeapon', weaponLabel, 4, 2, 1, simulation);
    },

    deleteWeapon(weapon) {
      weapon.destroyRecord();
    },

    updateWeaponSpecialRules(weapon, weaponSpecialRules) {
      weapon.set('specialRules', weaponSpecialRules);
      weapon.save();
    }
  },

});
