import DS from 'ember-data';
import { computed } from '@ember/object';
import { equal } from '@ember/object/computed';


export default DS.Model.extend({
  label: DS.attr('string'),
  weapons: DS.hasMany('weapon', {async: true, dependent: 'destroy'}),
  target: DS.belongsTo('target', {async: true, autoSave: true}),
  result: DS.belongsTo('result', {async: true, autoSave: true}),

  hasOneWeapon: equal('weapons.length', 1),

  isFilled: computed('label', 'target.{armor,pd}',
    'weapons.@each.{label,lock,attack,damage}', function() {
    if (!this.get('label') ||
        !this.get('target.armor') ||
        !this.get('target.pd'))
      return false;

    function isWeaponFilled(weapon) {
      return weapon.get('label') &&
              weapon.get('lock') &&
              weapon.get('attack') &&
              weapon.get('damage');
    }

    return this.get('weapons').every(isWeaponFilled);
  }),

});
