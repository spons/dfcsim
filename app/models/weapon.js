import DS from 'ember-data';
import { computed } from '@ember/object';


export default DS.Model.extend({
  label: DS.attr('string'),
  lock: DS.attr('number'),
  attack: DS.attr('number'),
  damage: DS.attr('number'),
  specialRules: DS.hasMany('weapon-special-rule'),
  simulation: DS.belongsTo('simulation', {autoSave: true}),

  hasSpecialRulesConflict: computed('specialRules.[]', function(){
    function burnthroughFilter(specialRule) {
      return specialRule.get('name').startsWith('burnthrough-');
    }

    // No more than one Burnthrough
    return this.get('specialRules').filter(burnthroughFilter).get('length') > 1;
  }),
});
