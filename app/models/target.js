import DS from 'ember-data';

export default DS.Model.extend({
  armor: DS.attr('number'),
  pd: DS.attr('number'),
  simulation: DS.belongsTo('simulation', {async: true, autoSave: true}),
});
