import DS from 'ember-data';

export default DS.Model.extend({
  damageProbability: DS.attr(),
  simulation: DS.belongsTo('simulation', {async: true, autoSave: true}),
});
