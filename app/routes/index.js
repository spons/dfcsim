import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return {
      simulations: this.get('store').findAll(
        'simulation',
        {include: 'weapons,weapons.specialRules,target'}),

      weaponSpecialRules: this.get('store').findAll('weapon-special-rule'),

      results: this.get('store').findAll('result', {include: 'simulation'}),
    };
  }
});
