import Component from '@ember/component';
import { computed } from '@ember/object';
import { gt } from '@ember/object/computed';
import EmberObject from '@ember/object';

export default Component.extend({
  hasResults: gt('results.length',  0),

  chartData: computed('results.@each.{damageProbability,simulation}',
    function () {
      let datasets = [];
      this.get('results').forEach((result, index) => {
        let color = this.colors[index %  this.colors.length];
        datasets.push(
            EmberObject.create({
              label: result.get('simulation.label'),
              data: result.get('damageProbability').map(dmgProb =>
                EmberObject.create({x: dmgProb.dmg, y: dmgProb.p})),
              borderColor: 'rgba(' + color + ',1)',
              backgroundColor: 'rgba(' + color + ',0.2)',
            }));
        }
      );
      return { datasets: datasets };
  }),

  init() {
    this._super(...arguments);

    this.colors= [
      '0, 0, 0',
      '230, 159, 0',
      '86, 180, 233',
      '0, 158, 115',
      '240, 228, 66',
      '0, 114, 178',
      '213, 94, 0',
      '204, 121, 167',
    ];

    this.chartOptions = {
      elements: {
        line: {
          tension: 0, // disables bezier curves
        }
      },
      scales: {
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Damage',
          },
          type: 'linear',
          ticks: {
            beginAtZero: true,
          }
        }],
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Probability (%)',
          },
          type: 'linear',
          ticks: {
            beginAtZero: true,
          }
        }]
      },
    };
  },
});
