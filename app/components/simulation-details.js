import Component from '@ember/component';
import { computed } from '@ember/object';
import { not, or } from '@ember/object/computed';
import { A } from '@ember/array';

export default Component.extend({
  armor_options: A([1, 2, 3, 4, 5, 6]),
  pd_options: A([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),

  hasSpecialRulesConflict:
    computed('simulation.weapons.@each.hasSpecialRulesConflict', function () {
      return this.get('simulation.weapons').isAny('hasSpecialRulesConflict',
                                                  true);
    }),

  isEditable: true,

  isNotEditable: not('isEditable'),

  isSimulationNotFilled: not('simulation.isFilled'),

  isNotValidated: or('isSimulationNotFilled', 'hasSpecialRulesConflict'),

  actions: {
    clickSaveEdit(simulation) {
      if (this.get('isEditable')) {
        this.get('saveSimulation')(simulation);
      }

      this.toggleProperty('isEditable');
    },

    clickAddWeapon(simulation) {
      this.get('addWeapon')(simulation);
    },

    deleteWeapon(weapon) {
      this.get('deleteWeapon')(weapon);
    },

    clickDeleteSimulation(simulation) {
      this.get('deleteSimulation')(simulation);
    },

    updateWeaponSpecialRules(weapon, weaponSpecialRule) {
      this.get('updateWeaponSpecialRules')(weapon, weaponSpecialRule);
    },

  }
});
