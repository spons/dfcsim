import Component from '@ember/component';
import { equal } from '@ember/object/computed';

export default Component.extend({
  didReceiveAttrs() {
    this._super(...arguments);
    if (this.get('simulations').get('length') === 0) {
      this.get('createSimulation')();
    }
  },

  isLastSimulation: equal('simulations.length', 1),

  actions: {
    saveSimulation(simulation) {
      this.get('saveSimulation')(simulation);
    },

    addWeapon(simulation) {
      this.get('addWeapon')(simulation);
    },

    deleteWeapon(weapon) {
      this.get('deleteWeapon')(weapon);
    },

    deleteSimulation(simulation) {
      this.get('deleteSimulation')(simulation);
    },

    updateWeaponSpecialRules(weapon, weaponSpecialRule) {
      this.get('updateWeaponSpecialRules')(weapon, weaponSpecialRule);
    },
  },
});
