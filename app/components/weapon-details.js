import Component from '@ember/component';
import { not, or } from '@ember/object/computed';
import { A } from '@ember/array';

export default Component.extend({
  tagName: 'tr',

  lock_options: A([1, 2, 3, 4, 5, 6]),
  attack_options: A([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]),
  damage_options: A([1, 2, 3, 4, 5, 6]),


  isNotEditable: not('isEditable'),

  isNotDeletable: or('isNotEditable', 'isLastWeapon'),

  actions: {
    clickDeleteWeapon(weapon) {
      this.get('deleteWeapon')(weapon);
    },

    selectWeaponSpecialRules(weapon, weaponSpecialRule) {
      this.get('updateWeaponSpecialRules')(weapon, weaponSpecialRule);
    },
  },

});
